# Importation bibliothèques Python

from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.preprocessing import StandardScaler
from sklearn import svm
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import numpy as np
from config import get_label_dataframe, get_trip_dataframe, get_label_test_dataframe, get_trip_test_dataframe
import pandas as pd
import seaborn as sns


# SVM_method(liste des données, Label, (noyau selectionnée, paramètre), taille échantillon de test) : Fonction qui retourne les résultats d'accuracy pour la SVM à n dimensions. Pour le cas du noyau poly, mettre n'importe quoi pour le paramètre.

def SVM_method_accuracy(liste_variables: list, label: list, noyau: tuple = ('linear', 0.1), taille_test: str = 0.2):
    Variables_train, Variables_test, Label_train, Label_test = train_test_split(
        liste_variables, label, test_size=taille_test, random_state=0)
    # Normalisation des variables
    scaler = StandardScaler()
    Variables_train = scaler.fit_transform(Variables_train)
    Variables_test = scaler.transform(Variables_test)
    # détermination du modèle
    type_noyau, parametre = noyau
    if type_noyau == 'linear':
        model = svm.SVC(kernel=type_noyau, C=parametre)
    elif type_noyau == 'poly':
        model = svm.SVC(kernel=type_noyau)
    elif type_noyau == 'rbf':
        model = svm.SVC(kernel=type_noyau, gamma=parametre)
    # Entrainement du modèle
    model.fit(Variables_train, Label_train)
    # Prédiction des labels grâce au modèle
    Label_train_pred = model.predict(Variables_train)
    Label_test_pred = model.predict(Variables_test)
    # Calcul des accuracy sur les variables d'entraînement et de test
    train_accuracy = accuracy_score(Label_train, Label_train_pred)
    test_accuracy = accuracy_score(Label_test, Label_test_pred)
    # Calcul de la matrice de confusion
    train_cm = confusion_matrix(Label_train, Label_train_pred)
    print("Matrice de confusion pour les données d'entraînement :")
    print(train_cm)
    test_cm = confusion_matrix(Label_test, Label_test_pred)
    print("Matrice de confusion pour les données de test :")
    print(test_cm)
    return train_accuracy, test_accuracy

# SVM_method_PCA(liste des données, Label, (noyau selectionnée, paramètre), taille échantillon de test, affichage ou non) : Fonction qui retourne les résultats d'accuracy pour la SVM en 2D grâce à la PCA. Pour le cas du noyau poly, mettre n'importe quoi pour le paramètre.


def SVM_method_PCA_accuracy(liste_variables: list, label: list, noyau: tuple = ('linear', 0.1), taille_test: str = 0.2, print_output: bool = False):
    Variables_train, Variables_test, Label_train, Label_test = train_test_split(
        liste_variables, label, test_size=taille_test, random_state=0)
    # Normalisation des variables
    scaler = StandardScaler()
    Variables_train = scaler.fit_transform(Variables_train)
    Variables_test = scaler.fit_transform(Variables_test)
    # Passage aux variables PCA
    pca = PCA(n_components=2)  # Nombre de compososantes principales
    Variables_train_pca = pca.fit_transform(Variables_train)
    Variables_test_pca = pca.fit_transform(Variables_test)
    # détermination du modèle
    type_noyau, parametre = noyau
    if type_noyau == 'linear':
        model = svm.SVC(kernel=type_noyau, C=parametre)
    elif type_noyau == 'poly':
        model = svm.SVC(kernel=type_noyau)
    elif type_noyau == 'rbf':
        model = svm.SVC(kernel=type_noyau, gamma=parametre)
    # Entrainement du modèle
    model.fit(Variables_train_pca, Label_train)
    # Prédiction des labels grâce au modèle
    Label_train_pred = model.predict(Variables_train_pca)
    Label_test_pred = model.predict(Variables_test_pca)
    # Calcul des accuracy sur les variables d'entraînement et de test
    train_accuracy = accuracy_score(Label_train, Label_train_pred)
    test_accuracy = accuracy_score(Label_test, Label_test_pred)
    # Calcul de la matrice de confusion
    train_cm = confusion_matrix(Label_train, Label_train_pred)
    print("Matrice de confusion pour les données d'entraînement :")
    print(train_cm)
    test_cm = confusion_matrix(Label_test, Label_test_pred)
    print("Matrice de confusion pour les données de test :")
    print(test_cm)
    # Tracé
    if print_output == True:
        Var_x_pca = [Variables_train_pca[i][0] for i in range(len(
            Variables_train_pca))] + [Variables_test_pca[i][0] for i in range(len(Variables_test_pca))]
        Var_y_pca = [Variables_train_pca[i][1] for i in range(len(
            Variables_train_pca))] + [Variables_test_pca[i][1] for i in range(len(Variables_test_pca))]
        x_min, x_max = min(Var_x_pca) - 1, max(Var_x_pca) + 1
        y_min, y_max = min(Var_y_pca) - 1, max(Var_y_pca) + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.02),
                             np.arange(y_min, y_max, 0.02))
        Z = model.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)
        plt.contourf(xx, yy, Z, alpha=0.8)
        plt.scatter(Var_x_pca, Var_y_pca, c=Label_train+Label_test)
        plt.xlabel('Composante 1 PCA')
        plt.ylabel('Composante 2 PCA')
        plt.title('SVM Decision Boundary')
        plt.show()
    return train_accuracy, test_accuracy

# traitement_donnee(choix du max, min ou moyenne, nom des données utilisées) : renvoie les max, min ou les moyennes des 81 données mesurées dans un dataframe. En argumant, mettre 'max', 'min', 'mean'. La colonne label sera converti en 0 et 1 au lieu de True or False


def traitement_donnee(choix: str, nom_donnees_mesurees, test: bool = False):
    dictionnaire = {}
    for donnee in nom_donnees_mesurees:
        dictionnaire[f'{choix}_{donnee}'] = []
    dictionnaire['label'] = []
    if test == True:
        labels = get_label_test_dataframe()
        for i in range(labels.shape[0]):
            id_trip, label = labels.iloc[i]
            trip = get_trip_test_dataframe(id_trip)
            for donnee in nom_donnees_mesurees:
                if choix == 'max':
                    dictionnaire[f'{choix}_{donnee}'].append(
                        trip[donnee].max())
                elif choix == 'min':
                    dictionnaire[f'{choix}_{donnee}'].append(
                        trip[donnee].min())
                else:
                    dictionnaire[f'{choix}_{donnee}'].append(
                        trip[donnee].mean())
            dictionnaire['label'].append(0)
        dataframe = pd.DataFrame(dictionnaire)
    else:
        labels = get_label_dataframe()
        for i in range(labels.shape[0]):
            id_trip, label = labels.iloc[i]
            trip = get_trip_dataframe(id_trip)
            for donnee in nom_donnees_mesurees:
                if choix == 'max':
                    dictionnaire[f'{choix}_{donnee}'].append(
                        trip[donnee].max())
                elif choix == 'min':
                    dictionnaire[f'{choix}_{donnee}'].append(
                        trip[donnee].min())
                else:
                    dictionnaire[f'{choix}_{donnee}'].append(
                        trip[donnee].mean())
            dictionnaire['label'].append(label)
        dataframe = pd.DataFrame(dictionnaire)
        dataframe['label'] = dataframe['label'].astype('category').cat.codes
    return dataframe

# corr_sup_taux(dataframe des grandeurs : dataframe, taux de corrélation minimum, affichage du graphe ou non) : renvoie la liste des grandeurs traitées où la corrélation avec les labels est supérieur au coefficient


def corr_sup_taux(dataframe, taux_corr: float, print: bool = False):
    dataframe_correlation = dataframe.corrwith(
        dataframe['label']).drop(['label'], axis=0)
    nom_var_corr = dataframe_correlation.index.tolist()
    corr_sup = [[], []]
    corr_inf = [[], []]
    for i in range(dataframe_correlation.shape[0]):
        nom_variable = nom_var_corr[i]
        correlation = abs(dataframe_correlation)[i]
        if correlation >= taux_corr:
            corr_sup[0].append(nom_variable)
            corr_sup[1].append(correlation)
        else:
            corr_inf[0].append(nom_variable)
            corr_inf[1].append(correlation)
    if print == True:
        plt.scatter(corr_sup[0], corr_sup[1], color='r',
                    marker='o', label=f'corrélation > {taux_corr}')
        plt.scatter(corr_inf[0], corr_inf[1], color='b',
                    marker='o', label=f'corrélation < {taux_corr}')
        plt.scatter(nom_var_corr, [taux_corr] *
                    len(nom_var_corr), color='k', marker='.', s=10)
        plt.xticks(rotation='vertical')
        plt.title('Corrélation des paramètres avec les labels')
        plt.ylabel('Corrélation')
        plt.legend()
        plt.show()
    return corr_sup, corr_inf

# calcul_correlation_interparametre(dataframe des grandeurs, taux de correlation minimum, affichage du graphe des correlation avec les labels, affichage de la heatmap de la correlation interparamètres): isole les corrélations entre le paramètre dont la corrélation avec les labels est supérieur au taux de corrélation minimum.


def calcul_correlation_interparametre(dataframe, taux_corr: float, print_graphe: bool = True, print_heatmap: bool = True):
    # on enlève la colonne label du dataframe pour les correlations interparamètres
    dataframe_sans_label = dataframe.drop(['label'], axis=1)
    correlation_variables = dataframe_sans_label.corr()
    corr_inf = corr_sup_taux(dataframe, taux_corr, print=print_graphe)[1]
    for parametre in corr_inf[0]:
        correlation_variables = correlation_variables.drop(
            parametre, axis=0)
        correlation_variables = correlation_variables.drop(
            parametre, axis=1)
    nom_parametre_retenu = correlation_variables.columns.tolist()
    if print_heatmap == True:
        fig, ax = plt.subplots(figsize=(12, 9))
        sns.heatmap(correlation_variables, vmax=1, square=True)
        plt.show()
    return nom_parametre_retenu, correlation_variables

# valeurs_parametre_label(liste_noms_données, choix min/max/mean) : renvoie la liste des données sélectionnées ainsi que la liste des labels, pour la méthode SVM


def valeurs_parametre_label(dataframe, liste_param_choisi: list, choix: str):
    liste_données = []
    for i in range(0, dataframe['label'].shape[0]):
        liste_données.append([dataframe[parametre].iloc[i]
                             for parametre in liste_param_choisi])
    liste_label = dataframe['label'].tolist()
    return [liste_données, liste_label]

# SVM_modele((noyau selectionnée, paramètre)) : Fonction qui retourne le modèle . Pour le cas du noyau poly, mettre n'importe quoi pour le paramètre.


def SVM_modele(noyau: tuple = ('linear', 0.1)):
    # détermination du modèle
    type_noyau, parametre = noyau
    if type_noyau == 'linear':
        model = svm.SVC(kernel=type_noyau, C=parametre)
    elif type_noyau == 'poly':
        model = svm.SVC(kernel=type_noyau)
    elif type_noyau == 'rbf':
        model = svm.SVC(kernel=type_noyau, gamma=parametre)
    # Entrainement du modèle
    return model

# prediction(Liste des données pour le modèle, Liste des labels pour le modèle, liste des grandeurs utilisés par le modèle): retourne la prédiction par le modèle pour les différents liste de données


def prediction(liste_donnee, liste_label, liste_testee, noyau=('linear', 0.1)):
    model = SVM_modele(noyau)
    scaler = StandardScaler()
    liste_donnee = scaler.fit_transform(liste_donnee)
    liste_testee = scaler.fit_transform(liste_testee)
    model.fit(liste_donnee, liste_label)
    label_prediction = model.predict(liste_testee)
    return label_prediction
