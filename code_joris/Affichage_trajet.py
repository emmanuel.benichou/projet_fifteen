import pandas as pd
import os
import matplotlib.pyplot as plt

# récupération des .csv des trajets et des labels (mettre ce code dans le fichier code, au même endroit que config)
from config import get_label_dataframe, get_trip_dataframe

labels = get_label_dataframe()

# id de tous les trajets avec chutes
id_trip_fall = labels.trip_id[labels.label == True]
# id de tous les trajets sans chutes
id_trip_notfall = labels.trip_id[labels.label == False]


# trace les courbes des trajets d'ids id1 et id2 pour le paramètre selectionné
def tracer_courbe(parametre: str, id1: str, id2: str):

    trip_1 = get_trip_dataframe(id1)
    trip_2 = get_trip_dataframe(id2)

    fig, ax = plt.subplots()
    trip_1.plot(x="timestamp", y=parametre, ax=ax, grid=True,
                color='b')  # ax[0] refers to the first subplot axis
    # ax[1] refers to the second subplot axis
    trip_2.plot(x="timestamp", y=parametre, ax=ax, grid=True, color='r')
    ax.set(title=f"Features {parametre} for trips {id1} and {id2}")
    plt.show()


def comparaison_fall_nofall_max_parametre(parametre: str):
    trip_number_fall = []
    trip_number_nofall = []
    max_parametre_fall = []
    max_parametre_nofall = []
    for i in range(292):
        trip_id, label = labels.iloc[i]
        trip = get_trip_dataframe(trip_id)
        max = trip[parametre].max()
        if label == True:
            trip_number_fall.append(i)
            max_parametre_fall.append(max)
        else:
            trip_number_nofall.append(i)
            max_parametre_nofall.append(max)
    plt.scatter(trip_number_fall, max_parametre_fall, color='r', marker='.')
    plt.scatter(trip_number_nofall, max_parametre_nofall,
                color='b', marker='.')
    plt.title(f'max({parametre}) pour les différents trajets')
    plt.grid()
    plt.show()


def comparaison_fall_nofall_min_parametre(parametre: str):
    trip_number_fall = []
    trip_number_nofall = []
    min_parametre_fall = []
    min_parametre_nofall = []
    for i in range(292):
        trip_id, label = labels.iloc[i]
        trip = get_trip_dataframe(trip_id)
        min = trip[parametre].min()
        if label == True:
            trip_number_fall.append(i)
            min_parametre_fall.append(min)
        else:
            trip_number_nofall.append(i)
            min_parametre_nofall.append(min)
    plt.scatter(trip_number_fall, min_parametre_fall, color='r', marker='.')
    plt.scatter(trip_number_nofall, min_parametre_nofall,
                color='b', marker='.')
    plt.title(f'min({parametre}) pour les différents trajets')
    plt.grid()
    plt.show()

Parametres = get_trip_dataframe(labels.trip_id.iloc[1]).columns[1:-1]

for parametre in Parametres:
    comparaison_fall_nofall_max_parametre(parametre)
    comparaison_fall_nofall_min_parametre(parametre)
