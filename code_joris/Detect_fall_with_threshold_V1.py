import pandas as pd
import os
import matplotlib.pyplot as plt

# récupération des .csv des trajets et des labels (mettre ce code dans le fichier code, au même endroit que config)
from config import get_label_dataframe, get_trip_dataframe

labels = get_label_dataframe()

# id de tous les trajets avec chutes
id_trip_fall = labels.trip_id[labels.label == True]
# id de tous les trajets sans chutes
id_trip_notfall = labels.trip_id[labels.label == False]

# On détermine des seuils pour pouvoir détecter les chutes grâces aux trajets avec chutes et sans chute

# Détermination du seuil pour AYGX_mean et AZ_mean

AYGX_max_seuil = []
AZ_max_seuil = []

for trip_id in id_trip_notfall:
    trip = get_trip_dataframe(trip_id)
    AYGX_max_seuil.append(trip.AYGX_mean.max())
    AZ_max_seuil.append(trip.AZ_mean.max())
AYGX_max_nofall = max(AYGX_max_seuil)
AZ_max_nofall = max(AZ_max_seuil)

for trip_id in id_trip_fall:
    trip = get_trip_dataframe(trip_id)
    AYGX_max_seuil.append(trip.AYGX_mean.max())
    AZ_max_seuil.append(trip.AZ_mean.max())
AYGX_max_fall = min(AYGX_max_seuil)
AZ_max_fall = min(AZ_max_seuil)

seuil_AYGX = (AYGX_max_fall + AYGX_max_nofall)/2
seuil_AZ = (AZ_max_nofall + AZ_max_fall)/2


def fall(id: str):
    trip = get_trip_dataframe(id)
    AYGX_max, AYGX_max_id = trip.AYGX_mean.max(), trip.AYGX_mean.idxmax()
    timestamp_max = trip.timestamp.iloc[AYGX_max_id]
    if abs(AYGX_max) >= seuil_AYGX:
        AZ_max_local = trip.AZ_mean[AYGX_max_id-10:AYGX_max_id+10].max()
        if AZ_max_local >= seuil_AZ:
            return True, timestamp_max
    return False, None


def test_fall():
    liste_true_false = []
    for id in labels.trip_id:
        liste_true_false.append(fall(id))
    return pd.DataFrame(liste_true_false)


def comparaison_realite():
    comparaison = []
    liste_erreurs = []
    nb_succes = 0
    liste_test = test_fall()
    for i in range(liste_test.shape[0]):
        trip_id = labels.trip_id.iloc[i]
        label = labels.label.iloc[i]
        label_test, timestamp = liste_test.iloc[i]
        comparaison.append((trip_id, label, label_test, timestamp))
        if label == label_test:
            nb_succes += 1
        else:
            liste_erreurs.append((trip_id, label, label_test, timestamp))

    return pd.DataFrame(comparaison), nb_succes, pd.DataFrame(liste_erreurs)


print(comparaison_realite())
