import os
import pandas as pd

BASE_DIRECTORY = os.path.dirname(os.path.realpath(__file__))
DATASET_DIRECTORY = os.path.join(BASE_DIRECTORY, "CS_dataset")
TRIPS_DIRECTORY = os.path.join(DATASET_DIRECTORY, "trips")
TEST_DIRECTORY = os.path.join(DATASET_DIRECTORY, "test")


def get_label_dataframe():
    return pd.read_csv(os.path.join(DATASET_DIRECTORY, "labels.csv"))


def get_trip_dataframe(trip_id: str):
    return pd.read_csv(os.path.join(TRIPS_DIRECTORY, f"{trip_id}.csv"))


def get_label_test_dataframe():
    return pd.read_csv(os.path.join(DATASET_DIRECTORY, "labels_test.csv"))


def get_trip_test_dataframe(trip_id: str):
    return pd.read_csv(os.path.join(TEST_DIRECTORY, f"{trip_id}.csv"))


def write_label_test_dataframe(dataframe):
    dataframe.to_csv(os.path.join(DATASET_DIRECTORY,
                     "labels_test.csv"), index=False)
