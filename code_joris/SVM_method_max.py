# Importation Bibliothèques

import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.utils import class_weight
import seaborn as sns
from sklearn.decomposition import PCA

# Importation des fonctions pour la SVM et de config

from SVM_functions import SVM_method_accuracy, SVM_method_PCA_accuracy, corr_sup_taux, prediction, calcul_correlation_interparametre, valeurs_parametre_label, traitement_donnee, SVM_modele
from config import get_label_dataframe, get_trip_dataframe, get_trip_test_dataframe, get_label_test_dataframe, write_label_test_dataframe

if __name__ == '__main__':

    # récupération des labels et de la dataframe des données mesurées

    labels = get_label_dataframe()
    nom_donnees_mesurees = get_trip_dataframe(
        labels.trip_id.iloc[1]).columns.drop('timestamp')

    # traitement des données (récupération des maximums) ainsi que des grandeurs les plus corrélés aux labels (et la dataframe des intercorrélations)
    # Affiche également le graphe des corrélations avec les labels et la heatmap des corrélations interparamètres
    dataframe_données = traitement_donnee('max', nom_donnees_mesurees)
    nom_grandeurs_sup_tauxcorr, dataframe_correlation_interparametre = calcul_correlation_interparametre(
        dataframe_données, 0.3, print_graphe=True, print_heatmap=True)

    # SVM sur l'ensemble des données

    liste_parametre = dataframe_données.columns.tolist()
    liste_donnee, label = valeurs_parametre_label(
        dataframe_données, liste_parametre, 'max')
    print(SVM_method_accuracy(liste_donnee, label, ('poly', 1), taille_test=0.2))

    # SVM avec PCA sur l'ensemble des données
    for noyau in [('linear', 0.1), ('poly', 0), ('rbf', 0.02)]:
        liste_parametre = dataframe_données.columns.tolist()
        liste_donnee, label = valeurs_parametre_label(
            dataframe_données, liste_parametre, 'max')
        print(SVM_method_PCA_accuracy(liste_donnee, label,
                                      noyau, taille_test=0.2, print_output=False))

    # SVM sur un jeu de données sélectionné

    liste_parametre = ['max_GX_max', 'max_AY_min', 'max_Gnorm_extremum', 'max_AYGX_min',
                       'max_AZ_median', 'max_AYGX_meandiff', 'max_AZ_min', 'max_Gnorm_std']
    liste_donnee, label = valeurs_parametre_label(
        dataframe_données, liste_parametre, 'max')
    print(SVM_method_accuracy(liste_donnee, label, ('poly', 0.02), taille_test=0.2))

    # SVM avec PCA sur un jeu de données sélectionné
    for noyau in [('linear', 0.1), ('poly', 0), ('rbf', 0.02)]:
        liste_parametre = ['max_GX_max', 'max_AY_min', 'max_Gnorm_extremum', 'max_AYGX_min',
                           'max_AZ_median', 'max_AYGX_meandiff', 'max_AZ_min', 'max_Gnorm_std']
        liste_donnee, label = valeurs_parametre_label(
            dataframe_données, liste_parametre, 'max')
        print(SVM_method_PCA_accuracy(liste_donnee, label,
                                      noyau, taille_test=0.2, print_output=False))

    # Prédiction sur la nouvelle banque de donnée test

    # dataframe_donnee_test = traitement_donnee(
    #     'max', nom_donnees_mesurees, test=True)

    # liste_parametre = ['max_GX_max', 'max_AY_min', 'max_Gnorm_extremum', 'max_AYGX_min',
    #                    'max_AZ_median', 'max_AYGX_meandiff', 'max_AZ_min', 'max_Gnorm_std']

    # liste_donnée_model, label_model = valeurs_parametre_label(
    #     dataframe_données, liste_parametre, 'max')

    # liste_donnee_test, label_test = valeurs_parametre_label(
    #     dataframe_donnee_test, liste_parametre, 'max')

    # label_test = prediction(liste_donnée_model, label_model,
    #                         liste_donnee_test, ('rbf', 0.02))

    # labels = get_label_test_dataframe()
    # labels['label'] = pd.Series(label_test)
    # labels['label'] = labels['label'].astype(bool)
    # write_label_test_dataframe(labels[['trip_id', 'label']])
