import pandas as pd
import numpy as np
import os

# Chemin vers le dossier contenant les fichiers CSV
dossier_csv = "/Users/emmanuelbenichou/Desktop/Projet 2023 - fall detection/code/CS_dataset/trips"

# Chemin vers le fichier CSV contenant les identifiants et les étiquettes
fichier_labels = "/Users/emmanuelbenichou/Desktop/Projet 2023 - fall detection/code/CS_dataset/labels.csv"  

max_list = []

df_labels = pd.read_csv(fichier_labels)

for fichier in os.listdir(dossier_csv):
    if fichier.endswith(".csv"):
        identifiant = fichier[:-4]  # Exclure l'extension ".csv" pour ne garder que l'identifiant
        etiquette = df_labels.loc[df_labels['trip_id'] == identifiant, 'label'].values[0]
        # Loc me permet de filtrer les lignes où la colonne 'trip_id' == identifiant puis prend la colonne 'label' 
        # associée à ces lignes et en prend finalement la première valeur qui est bien l'étiquette

        if str(etiquette) == "True":

            df = pd.read_csv(os.path.join(dossier_csv, fichier))

            maximum = np.max(df["AYGX_mean"])

            max_list.append(maximum)

# Calcul du min des maxs
seuil = np.min(max_list)

print("Seuil choisi :", seuil)
