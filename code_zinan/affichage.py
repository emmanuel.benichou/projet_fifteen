import pandas as pd
import os
import matplotlib.pyplot as plt
from config import get_label_dataframe, get_trip_dataframe

labels = get_label_dataframe()
# Here we keep only the rows where label is equal to True
labels_fall = labels[labels.label == True]
labels_normal = labels[labels.label == False]


# here we take the first row of the dataframe labels_fall (note: it becomes a pandas series)
first_fall_dataframe = labels_fall.iloc[0]
first_fall_trip_id = first_fall_dataframe["trip_id"]
# Get the trip id of the falls as a list of strings &
trip_dataframe = get_trip_dataframe(first_fall_trip_id)


# Store the trip id of all the trips with/without a fall in a list
list_fall_trip_id = list(labels_fall.trip_id)
list_normal_trip_id = list(labels_normal.trip_id)

SIGNALS = ["AX", "AY", "AZ", "GX", "GY", "GZ"]
FUNCTIONS = ["mean", "min", "max", "median", "std",
             "meandiff", "mediandiff", "extremum", "delta"]
features_of_interest = [signal + "_" +
                        function for signal in SIGNALS for function in FUNCTIONS]

feature = "AYGX_mean"


def get_label(trip_id):
    label_real = labels.label[labels.trip_id == trip_id].bool()
    return label_real


def affichage(trip_id, feature):
    label_real = labels.label[labels.trip_id == trip_id].bool()
    df = get_trip_dataframe(trip_id)
    fig, ax = plt.subplots()
    df.plot(x="timestamp", y=feature, ax=ax, grid=True)
    if label_real:
        fall_normal = "fall"
    else:
        fall_normal = "normal"
    ax.set(xlabel="Timestamp (s)", ylabel=feature,
           title=f"Feature {feature} for a {fall_normal} trip ")
    plt.show()


def affichage_df(trip_id, df, feature):
    fig, ax = plt.subplots()
    df.plot(x="timestamp", y=feature, ax=ax, grid=True)
    if get_label(trip_id):
        fall_normal = "fall"
    else:
        fall_normal = "normal"
    ax.set(xlabel="Timestamp (s)", ylabel=feature,
           title=f"Feature {feature} for a {fall_normal} trip")
    plt.show()


def affichage_2features_subplot(trip_id, feature_1, feature_2):
    label_real = labels.label[labels.trip_id == trip_id].bool()
    df = get_trip_dataframe(trip_id)
    fig, ax = plt.subplots(2)
    df.plot(x="timestamp", y=feature_1, ax=ax[0], grid=True, color='b')
    df.plot(x="timestamp", y=feature_2, ax=ax[1], grid=True, color='r')
    if label_real:
        fall_normal = "fall"
    else:
        fall_normal = "normal"
    ax[0].set(
        title=f"Features {feature_1} and {feature_2} for {fall_normal}  trip {trip_id}")
    plt.show()


def affichage_2trips_subplot(trip_id1, trip_id2, feature):
    df1 = get_trip_dataframe(trip_id1)
    df2 = get_trip_dataframe(trip_id2)
    fig, ax = plt.subplots(2)
    df1.plot(x="timestamp", y=feature, ax=ax[0], grid=True, color='b')
    df2.plot(x="timestamp", y=feature, ax=ax[1], grid=True, color='r')
    ax[0].set(
        title=f"Features {feature} for a fall trip and a non-fall trip")
    plt.show()


def affichage_all_features_subplot(trip_id):
    # 根据trip_id筛选数据
    df = get_trip_dataframe(trip_id)

    # 创建子图
    fig, axes = plt.subplots(2, 1, figsize=(10, 8))

    # 第一个子图
    axes[0].plot(df['timestamp'], df['AX_mean'], label='AX_mean')
    axes[0].plot(df['timestamp'], df['AY_mean'], label='AY_mean')
    axes[0].plot(df['timestamp'], df['AZ_mean'], label='AZ_mean')
    axes[0].legend()
    axes[0].set_xlabel('Time')
    axes[0].set_ylabel('Acceleration')

    # 第二个子图
    axes[1].plot(df['timestamp'], df['GX_mean'], label='GX_mean')
    axes[1].plot(df['timestamp'], df['GY_mean'], label='GY_mean')
    axes[1].plot(df['timestamp'], df['GZ_mean'], label='GZ_mean')
    axes[1].legend()
    axes[1].set_xlabel('Time')
    axes[1].set_ylabel('Gyroscope')

    # 设置图形标题
    fig.suptitle(f'Trip {trip_id} Features')

    # 显示图形
    plt.show()

# trip_id1 = list_fall_trip_id[1]
# trip_id2 = list_normal_trip_id[1]
# affichage_2trips_subplot(trip_id1, trip_id2, "GX_mean")
