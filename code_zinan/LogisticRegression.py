import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.impute import SimpleImputer
from Aproche_phys import AYGX_mean_max, AZ_meandiff_on_AYGX_mean_max

# 读取数据
data = pd.read_csv('code/CS_dataset/labels.csv')

# 读取自行车运动数据文件夹trips中的所有文件
file_path = 'code/CS_dataset/trips/'
trip_data = []  # 创建一个空列表
labels = []

for index, row in data.iterrows():
    trip_id = row['trip_id']
    file_name = file_path + str(trip_id) + '.csv'
    trip = pd.read_csv(file_name)
    trip_data.append([AYGX_mean_max(trip), AZ_meandiff_on_AYGX_mean_max(trip)])
    labels.append(row['label'])

# 将自行车运动数据和标签合并
data['trip_data'] = trip_data
data['label'] = labels

# 提取特征和标签
X = data['trip_data'].tolist()
y = data['label']

# 划分训练集和测试集
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# 处理缺失值
imputer = SimpleImputer(strategy='mean')
X_train = imputer.fit_transform(X_train)

# 创建逻辑回归模型
model = LogisticRegression()

# 使用交叉验证计算多个指标
cv_scores = cross_val_score(model, X_train, y_train, cv=5, scoring='accuracy')
precision = cross_val_score(model, X_train, y_train, cv=5, scoring='precision')
recall = cross_val_score(model, X_train, y_train, cv=5, scoring='recall')
f1 = cross_val_score(model, X_train, y_train, cv=5, scoring='f1')

# 训练模型
model.fit(X_train, y_train)

# 预测
X_test = imputer.transform(X_test)
y_pred = model.predict(X_test)

# 计算预测准确率
accuracy = accuracy_score(y_test, y_pred)

print("accuracy_score:", accuracy)

# 打印结果
print("Cross-validation accuracy:", cv_scores)
print("Average accuracy:", cv_scores.mean())
print("Cross-validation precision:", precision)
print("Average precision:", precision.mean())
print("Cross-validation recall:", recall)
print("Average recall:", recall.mean())
print("Cross-validation F1-score:", f1)
print("Average F1-score:", f1.mean())
print("Test set accuracy:", accuracy)
