import pandas as pd
import os
import matplotlib.pyplot as plt
from config import *
from affichage import *

MAX = 1e100
MIN = -1e100

labels = get_label_dataframe()
# Here we keep only the rows where label is equal to True
labels_fall = labels[labels.label == True]
labels_normal = labels[labels.label == False]

# Store the trip id of all the trips with/without a fall in a list
list_trip_id = list(labels.trip_id)
list_fall_trip_id = list(labels_fall.trip_id)
list_normal_trip_id = list(labels_normal.trip_id)

SIGNALS = ["AX", "AY", "AZ", "GX", "GY", "GZ", "AYGX"]
FUNCTIONS = ["mean", "min", "max", "median", "std",
             "meandiff", "mediandiff", "extremum", "delta"]
features_of_interest = [signal + "_" +
                        function for signal in SIGNALS for function in FUNCTIONS]

# AYGX_mean in colomn 55
seuil_AYGX_mean_max = MAX
seuil_AYGX_mean_min = MIN

for trip_id in list_fall_trip_id:
    df = get_trip_dataframe(trip_id)
    column = df["AYGX_mean"]
    seuil_AYGX_mean_max = min(seuil_AYGX_mean_max, column.max())
    seuil_AYGX_mean_min = max(seuil_AYGX_mean_min, column.min())

# print(seuil_AYGX_mean_max, seuil_AYGX_mean_min)

# AYGX_min in colomn 56
seuil_AYGX_min = MIN

for trip_id in list_fall_trip_id:
    df = get_trip_dataframe(trip_id)
    column = df["AYGX_min"]
    seuil_AYGX_min = max(seuil_AYGX_min, column.min())

# AYGX_max in colomn 57
seuil_AYGX_max = MAX

for trip_id in list_fall_trip_id:
    df = get_trip_dataframe(trip_id)
    column = df["AYGX_max"]
    seuil_AYGX_max = min(seuil_AYGX_max, column.max())

seuil_AYGX_median = MAX
for trip_id in list_fall_trip_id:
    df = get_trip_dataframe(trip_id)
    column = df["AYGX_median"]
    seuil_AYGX_median = min(seuil_AYGX_median, column.max())

seuil_AYGX_median = 400

# 现在的表现最好0.9315068493150684


def seuil_AYGX_mean_estime(df):
    column = df["AYGX_mean"]
    max_index = df["AYGX_mean"].idxmax()
    AZ_meandiff = df["AZ_meandiff"][max_index]
    if (column.max() > seuil_AYGX_mean_max and df[column].min() < seuil_AYGX_mean_min):
        label_estime = True
    else:
        label_estime = False
    return label_estime


def seuil_AYGX_min_estime(df):
    column = df["AYGX_min"]
    if (column.min() < seuil_AYGX_min):
        label_estime = True
    else:
        label_estime = False
    return label_estime


def seuil_AYGX_max_estime(df):
    column = df["AYGX_max"]
    if (column.max() > seuil_AYGX_max):
        label_estime = True
    else:
        label_estime = False
    return label_estime


def seuil_AYGX_median_estime(df):
    column = df["AYGX_mean"]
    if (column.max() > seuil_AYGX_median):
        label_estime = True
    else:
        label_estime = False
    return label_estime


# estimation
cmt = 0
cmt_ture = 0
falseEstime_id = []
for trip_id in list(list_trip_id):
    cmt += 1
    df = get_trip_dataframe(trip_id)
    label_real = labels.label[labels.trip_id == trip_id].bool()
    label_estime = seuil_AYGX_mean_estime(
        df)
    if (label_estime == label_real):
        cmt_ture += 1
    else:
        falseEstime_id.append(trip_id)

Taux_prediction = cmt_ture/cmt
print(Taux_prediction)
