import pandas as pd
import os
import matplotlib.pyplot as plt
from config import *
from affichage import *

MAX = 1e100
MIN = -1e100

labels = get_label_dataframe()
# Here we keep only the rows where label is equal to True
labels_fall = labels[labels.label == True]
labels_normal = labels[labels.label == False]

# Store the trip id of all the trips with/without a fall in a list
list_trip_id = list(labels.trip_id)
list_fall_trip_id = list(labels_fall.trip_id)
list_normal_trip_id = list(labels_normal.trip_id)

SIGNALS = ["AX", "AY", "AZ", "GX", "GY", "GZ", "AYGX"]
FUNCTIONS = ["mean", "min", "max", "median", "std",
             "meandiff", "mediandiff", "extremum", "delta"]
features_of_interest = [signal + "_" +
                        function for signal in SIGNALS for function in FUNCTIONS]

# AYGX_mean in colomn 55
seuil_AYGX_mean_max = MAX
seuil_AYGX_mean_min = MIN

for trip_id in list_fall_trip_id:
    df = get_trip_dataframe(trip_id)
    column = df["AYGX_mean"]
    seuil_AYGX_mean_max = min(seuil_AYGX_mean_max, column.max())
    seuil_AYGX_mean_min = max(seuil_AYGX_mean_min, column.min())

# print(seuil_AYGX_mean_max, seuil_AYGX_mean_min)


def AYGX_mean_max(df):
    column = df["AYGX_mean"]
    return column.max()


def AZ_meandiff_on_AYGX_mean_max(df):
    max_index = df["AYGX_mean"].idxmax()
    AZ_meandiff = df["AZ_meandiff"][max_index]
    return AZ_meandiff


def affichage_distribution():
    x = []
    y = []
    colors = []
    for trip_id, label in zip(labels['trip_id'], labels['label']):
        df = get_trip_dataframe(trip_id)
        xi = AYGX_mean_max(df)
        yi = AZ_meandiff_on_AYGX_mean_max(df)
        x.append(xi)
        y.append(yi)
        color = 'red' if label == 1 else 'blue'
        colors.append(color)

    fig, ax = plt.subplots()
    ax.set_title('Scatter Plot')
    ax.set_xlabel('AYGX_mean_max')
    ax.set_ylabel('AZ_meandiff_on_AYGX_mean_max')
    ax.scatter(x, y, c=colors)
    plt.show()


def AZ_meandiff_on_AYGX_mean_max_estime(df):
    x = AYGX_mean_max(df)
    y = AZ_meandiff_on_AYGX_mean_max(df)
    if y <= x/100-4:
        label_estime = True
    else:
        label_estime = False
    return label_estime


affichage_distribution()

# estimation
cmt = 0
cmt_ture = 0
falseEstime_id = []
for trip_id in list(list_trip_id):
    cmt += 1
    df = get_trip_dataframe(trip_id)
    label_real = labels.label[labels.trip_id == trip_id].bool()
    label_estime = AZ_meandiff_on_AYGX_mean_max_estime(
        df)
    if (label_estime == label_real):
        cmt_ture += 1
    else:
        falseEstime_id.append(trip_id)

Taux_prediction = cmt_ture/cmt
print(Taux_prediction)
