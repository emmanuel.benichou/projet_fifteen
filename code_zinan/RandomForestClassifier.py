import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.impute import SimpleImputer

# 读取trips.csv文件
data = pd.read_csv('code/CS_dataset/labels.csv')

# 读取自行车运动数据文件夹trips中的所有文件
file_path = 'code/CS_dataset/trips/'
trip_data = []
labels = []
for index, row in data.iterrows():
    trip_id = row['trip_id']
    file_name = file_path + str(trip_id) + '.csv'
    trip = pd.read_csv(file_name)
    trip_data.append(trip)
    labels.append(row['label'])

# 将自行车运动数据和标签合并
data['trip_data'] = trip_data
data['label'] = labels

# 提取特征
threshold_AY_mean = 6
threshold_GX_mean = 40
threshold_AZ_mean = 0

X = []
y = []
for trip, label in zip(data['trip_data'], data['label']):
    # 假设'AY', 'GX', 'AYGX'为需要提取特征的列名
    features = trip[['AY_mean', 'GX_mean', 'AZ_mean']]
    # 检查特征是否有明显的峰值，如果有则设置为1，否则为0
    peak = ((features['AY_mean'] > threshold_AY_mean)).astype(int)
    X.append(peak)
    y.append(label)

# 转换为NumPy数组
X = pd.DataFrame(X)
y = pd.Series(y)


# 创建SimpleImputer对象，用0填充NaN值
imputer = SimpleImputer(strategy="constant", fill_value=0)

# 填充NaN值
X = imputer.fit_transform(X)
print(X)

# 划分训练集和测试集
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# 创建随机森林分类器模型
model = RandomForestClassifier()

# 训练模型
model.fit(X_train, y_train)

# 在测试集上进行预测
y_pred = model.predict(X_test)

# 计算准确率
accuracy = accuracy_score(y_test, y_pred)
print('Accuracy:', accuracy)
