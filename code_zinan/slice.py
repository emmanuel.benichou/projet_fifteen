import pandas as pd
import os
import matplotlib.pyplot as plt
from config import *
from affichage import *

MAX = 1e100
MIN = -1e100

labels = get_label_dataframe()
# Here we keep only the rows where label is equal to True
labels_fall = labels[labels.label == True]
labels_normal = labels[labels.label == False]

# Store the trip id of all the trips with/without a fall in a list
list_trip_id = list(labels.trip_id)
list_fall_trip_id = list(labels_fall.trip_id)
list_normal_trip_id = list(labels_normal.trip_id)

SIGNALS = ["AX", "AY", "AZ", "GX", "GY", "GZ", "AYGX"]
FUNCTIONS = ["mean", "min", "max", "median", "std",
             "meandiff", "mediandiff", "extremum", "delta"]
features_of_interest = [signal + "_" +
                        function for signal in SIGNALS for function in FUNCTIONS]


def slice(df):
    max_index = df["AYGX_mean"].idxmax()
    start_index = max_index - 10  # 以最大值为中点的前5个时间点
    end_index = max_index + 10   # 以最大值为中点的后5个时间点
    slice_df = df.iloc[start_index:end_index+1]
    return slice_df


for i in range(5):
    trip_id = list_fall_trip_id[i]
    df = get_trip_dataframe(trip_id)
    affichage_df(trip_id,df = slice(df),feature = "AYGX_mean")
    affichage_df(trip_id,df = slice(df),feature = "AZ_mean")
    affichage_df(trip_id,df = slice(df),feature = "AZ_meandiff")
